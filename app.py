from flask import Flask, render_template, request
from flask_restful import Resource, Api
from json import dumps
from flask_jsonpify import jsonify

app = Flask(__name__)
api = Api(app)

@app.route('/')
def hello_world():
  return 'Hello, World!'

@app.route('/user/<username>')
def show_user_profile(username):
  return f'User {username or "Anonymous"}'

@app.route('/hello/<name>')
def hello(name):
  return render_template('hello.html', name=(name or "Anonymous"))

class Info(Resource):
    def get(self):
        return {'version': 3, 'method': 'GET', 'message': 'Running'}

api.add_resource(Info, '/get_info')

if __name__ == '__main__':
     app.run(host='0.0.0.0', port='8080')
