FROM centos:7

RUN yum install python3 python3-pip -y
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN mkdir /python_api
ADD templates /python_api/templates
COPY app.py /python_api/app.py
CMD ["python3", "/python_api/app.py"]
